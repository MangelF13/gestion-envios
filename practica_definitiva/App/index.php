<?php


define("RUTA", realpath(__DIR__.'/..'));

$currentPath = $_SERVER['PHP_SELF'];
$pathInfo = pathinfo($currentPath); 
$url = 'https://'.$_SERVER['HTTP_HOST'].rtrim($pathInfo['dirname'], ' \/')."/";

define("URL_BASE", $url."Vistas/");
define("URL_ACT", 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

require_once(RUTA."/App/Config.php");
require_once(RUTA."/App/Librerias/MySql.php");
require_once(RUTA."/App/Librerias/ConexionBDD.php");
require_once(RUTA."/App/Modelos/ModeloEnvios.php");
require_once(RUTA."/App/Librerias/FiltroDatos.php");
require_once(RUTA."/App/Controladores/ControladorEnvios.php");


#####################################


$ruta = [ 'ingresar' => 'ingreso_envio', 
    'listar' => 'lista_envios', 
    'eliminar' => 'elimina_envio', 
    'anotar' => 'anota_recepcion', 
    'buscar' => 'busca_envios',
    'modificar' => 'modifica_envio'
    ];

if (isset($_GET['envio'])) {
    $param = $_GET['envio'];
    
    if (isset($ruta[$param])) {
        $metodo = $param."Envio";
        ControladorEnvios::$metodo();
    }
    else {
        header('Status: 404 Not Found');
        echo '<html><body><h1>Error 404: No existe la ruta <i>'.$_GET['envio'].'</i></h1></body></html>';
        exit;
    }
} 
else {
    unset($titulo);
    unset($cabecera);
    unset($contenido);
    require_once(RUTA."/App/Vistas/base.php");
}



