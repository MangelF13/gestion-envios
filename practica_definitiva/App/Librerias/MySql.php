<?php


class MySql {
    
    #Guarda internamente el objeto de conexión
    private $input;

    
    /**
     * Se conecta según los datos especificados
     *
     * @param $host
     * @param $usuario
     * @param $cont
     * @param $nombre_bdd
     *
     * @return \mysqli
     */
    public function conectar($host, $usuario, $cont, $nombre_bdd) {
        $this->input = @new mysqli($host, $usuario, $cont, $nombre_bdd);
        return $this->input;
    }

    /**
     * Ejecuta una consulta.
     *
     * @param $consulta
     *
     * @return bool|\mysqli_result
     */
    public function consulta($consulta) {
        return mysqli_query($this->input, $consulta);
    }

    /**
     * Devuelve si la base de datos esta conectado o no.
     *
     * @return bool
     */
    public function conectado() {
        return !is_null($this->input);
    }

    /**
     * Evita la inyeccion de codigo.
     *
     * @param $consulta
     *
     * @return string
     */
    public function escapar($consulta) {
        return $mysqli->real_escape_string($this->input, $consulta);
    }

    /**
     * Obtiene un array de la fila correspondiente.
     *
     * @param $resultado
     *
     * @return array|null
     */
    public function resultadoAsoc($resultado) {
        return mysqli_fetch_assoc($resultado);
    }

    /**
     * Obtiene el texto del error.
     *
     * @return string
     */
    public function error() {
        return mysqli_error($this->input);
    }

    /**
     * Obtiene el codigo de error.
     *
     * @return int
     */
    public function numError() {
        return mysqli_errno($this->input);
    }
    
    /**
     * Devuelve el indice del ultimo elemento insertado.
     *
     * @return mixed
     */
    public function insert_id() {
        return $this->input->insert_id;
    }
}