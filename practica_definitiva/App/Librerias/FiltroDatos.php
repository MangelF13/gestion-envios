<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FiltroDatos
 *
 * @author Mangel
 */
class FiltroDatos {
    
    /**
     * Introduce los d
     *
     * @param $datos
     *
     * @return bool
     */
    public static function filtrarEnvio($datos) {
        $error = [];
        foreach ($datos as $indice => $envio) {
            if(isset($envio[$indice])){
                if(($indice == 'destinatario' || $indice == 'poblacion') && self::validarString($envio[$indice] == FALSE)) {
                    $error[$indice."-error"] = $indice;
                }

                if($indice == 'telefono' && self::validarTelefono($envio[$indice] == FALSE)) {
                    $error[$indice."-error"] = $indice;
                }

                if($indice == 'direccion' && self::validarStringN($envio[$indice]) == FALSE) {
                    $error[$indice."-error"] = $indice;
                }

                if($indice == 'email' && self::validarEmail($envio[$indice]) == FALSE) {
                    $error[$indice."-error"] = $indice;
                }

                if($indice == 'c_postal' && self::validarCod_Postal($envio[$indice]) == FALSE) {
                    $error[$indice."-error"] = $indice;
                }

                if($indice == 'provincia' && self::validarCod_Provincia($envio[$indice]) == FALSE) {
                    $error[$indice."-error"] = $indice;
                }
            }
        }
        
        return $error;
    }

    

    /**
     * Valida el destinatario y la población (campos alfabéticos).
     *
     * @param $datos
     *
     * @return bool
     */
    public static function validarString($datos) {
        $string = "/^[A-z üÜáéíóúÁÉÍÓÚñÑ]{1,50}$/";
        
        if(preg_match($string, $datos)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    /**
     * Valida campos de texto que permite números (dirección).
     *
     * @param $datos
     *
     * @return bool
     */
    public static function validarStringN($datos) {
        $string = '/^[A-z 0-9 üÜáéíóúÁÉÍÓÚñÑ,.-ºª'."'".']{1,150}$/';
        
        if(preg_match($string, $datos)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    
    
    /**
     * Valida un email.
     *
     * @param $datos
     *
     * @return bool
     */
    public static function validarEmail($datos) {
        //Encontrado en: http://www.regular-expressions.info/email.html
        $string = '/[a-z0-9!#$%&'."'".'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'."'".'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/';
        
        if(preg_match($string, $datos)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    
    /**
     * Valida un número de teléfono.
     *
     * @param $datos
     *
     * @return bool
     */
    public static function validarTelefono($datos) {
        $string = '/^[A-z 0-9 üÜáéíóúÁÉÍÓÚñÑ,.-ºª'."'".']{1,150}$/';
        
        if(preg_match($string, $datos)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    
    /**
     * Valida el código postal.
     *
     * @param $datos
     *
     * @return bool
     */
    public static function validarCod_Postal($datos) {
        $cod_postal="/^(5[0-2]|[1-4][0-9]|0[1-9])[0-9]{3}$/";
        
        if(preg_match($string, $datos)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    
    public static function validarCod_Provincia($datos) {
        $provincia="/^(5[0-2]|[1-4][0-9]|0[1-9])$/";
        
        if(preg_match($string, $datos)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    
    public static function validarFecha($datos) {
        $fecha ="/^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/";
        //Para dd-mm-yyyy
        // /^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])-(19|20)\d\d$/
        
        if(preg_match($string, $datos)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
}
