<?php


class ConexionBDD {
    
    private $bdd;
    private $params;
    private static $_con;
    
    
    
    
    /**
     * Constructor de la capa de abstracion.
     * 
     *
     * @throws \Exception
     */
    private function __construct() {

        $this->bdd = new MySql;
        $this->bdd->conectar(Config::$servidor, Config::$usuario, Config::$contr, Config::$bd);

        if (!$this->bdd->conectado()) {
            die("No se ha podido conectar a la base de datos. "
            . "Vuelva a intentar la operacion: ". self::obtenerError());
        }
    }
    
    public static function crearConexion(){
        if (!self::$_con) {
            self::$_con = new self();
        }//TODO: comprobar
        
        return self::$_con;
    }
    
    /**
     * @param $coincidencias
     *
     * @return mixed
     */
    private function reemplazarParams(){
        $b=current($this->params);
        next($this->params);
        return $b;
    }

    /**
     * Prepara la consulta para evitar inyeccion de codigo.
     *
     * @param $sql
     * @param $params
     *
     * @return mixed
     */
    private function preparar($sql, $params) {
        for($i=0;$i<sizeof($params); $i++){
            if(is_bool($params[$i])) {
                //FALSE && FALSE == FALSE;
                $params[$i] = $params[$i]? TRUE : FALSE;
            }
            elseif(is_double($params[$i])) {
                $params[$i] = str_replace(',', '.', $params[$i]);
            }
            elseif(is_numeric($params[$i])) {
                $params[$i] = $this->bdd->escapar($params[$i]);
            }
            elseif(is_null($params[$i])) {
                $params[$i] = "NULL";
            }
            else {
                $params[$i] = "'".$this->bdd->escapar($params[$i])."'";
            }
        }

        $this->params = $params;
        $resultado = preg_replace_callback("/(\?)/i", array($this,"reemplazarParams"), $sql);

        return $resultado;
    }
    
    /**
     * Ejecuta una consulta que no devolvera valores.
     *
     * @param      $sql
     * @param null $params
     *
     * @return mixed
     */
    public function enviarConsulta($sql, $params = NULL) {
        $consulta = $this->preparar($sql, $params);
        $resultado = $this->bdd->consulta($consulta);
        if($this->bdd->numError()){
            //Controlar errores
        }
        
        return $resultado;
    }
    
    /**
     * Realiza una consulta que devolvera un array de valores
     *
     * @param      $sql
     * @param null $params
     *
     * @return array|NULL
     */
    public function ejecutar($sql, $params = NULL) {
        $resultado = $this->enviarConsulta($sql, $params);
        if(is_object($resultado)){
            $array = array();
            while($fila = $this->bdd->resultadoAsoc($resultado)){
                $array[] = $fila;
            }
            
            return $array;
        }
        
        return NULL;
    }
    
    /**
     * Realiza una consulta que devolvera un valor unico.
     *
     * @param      $sql
     * @param null $params
     *
     * @return mixed|null
     */
    public function ejecutarUnico($sql, $params = NULL) {
        $resultado = $this->enviarConsulta($sql, $params);
        if(!is_null($resultado)){
            if(!is_object($resultado)){
                return $resultado;
            }
            else{
                $fila = $this->bdd->resultadoAsoc($resultado);

                return $fila;
            }
        }
        
        return NULL;
    }
    
        
    /**
     * Obtiene el error correspondiente
     *
     * @return mixed
     */
    public function obtenerError() {
        return $this->bdd->error();
    }
    
    /**
     * Obtiene el indice del ultimo elemento
     *
     * @return mixed
     */
    public function ultimoId() {
        return $this->bdd->insert_id();
    }
}
