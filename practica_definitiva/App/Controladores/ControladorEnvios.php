<?php

/**
 * Class ControladorEnvios
 *
 * Contiene las diferentes funciones que controlan a los envíos.
 */
class ControladorEnvios {
    
    public $estilo;
    /**
     * Función que introduce el envío a la base de datos
     */
    public static function ingresarEnvio() {
        $modelo = new ModeloEnvios();
        
        $datos = array(
            'destinatario'=>'',
            'email' =>'',
            'telefono' =>'',
            'poblacion' =>'',
            'provincia' => '',
            'direccion' =>'',
            'c_postal' =>'',
            'observaciones' => ''
        );
        
        $provincias = $modelo->obtenerProvincias();
        
        
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            foreach ($datos as $campo => $post) {
                $datos[$campo] = $_POST[$campo];
            }
            $datos['estado'] = 'P';
            $datos['fecha_envio'] = '';
            
            
            $error_validacion = FiltroDatos::filtrarEnvio($datos);
            
            if(empty($error_validacion)) {
                $datos["mensaje"] = $modelo->insertarEnvio($datos);
            }
            else {
                $datos["mensaje"] = "Los datos introducidos no son válidos.";
            }
        }
        
        require_once (RUTA."/App/Vistas/ingreso_envio.php");
    }
    
    /**
     * Función que se encarga de mostrar la lista de envíos
     * 
     * @param type $condiciones
     */
    public static function listarEnvio($condiciones = NULL){
        $modelo = new ModeloEnvios();
        
        $condicionesSql = $modelo->obtenerCond($condiciones);
        $numeroEnvios = $modelo->obtenerNumeroEnvios($condicionesSql)["cantidad"];
        $tamano_pagina = 10;
        
        if(isset($_GET["pagina"])) {
            $pagina = $_GET["pagina"];
            $inicio = ($pagina - 1) * $tamano_pagina;
        }
        else {
            $inicio = 0;
            $pagina = 1;
        }
        
        $numeroPaginas = ceil($numeroEnvios / $tamano_pagina);
        $envios = $modelo->obtenerEnvios($inicio, $tamano_pagina, $condicionesSql);
        
        require_once (RUTA."/App/Vistas/lista_envios.php");
    }
    
    /**
     * Función que elimina el envío elegido.
     * 
     * Requiere la confirmación de la acción.
     */
    public static function eliminarEnvio(){
        $modelo = new ModeloEnvios();
        
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $cod = $_POST['eliminar'];
            $existe = $modelo->existeEnvio($cod);
            
            if($existe) {
                $accion = "confirmar";
            }
            else {
                $error = "no existe";
            }
            
            if(isset($_POST['codigo'])) {
                $confirmar = $_POST['confirmar'];
                
                if($confirmar == "Sí") {
                    $modelo->eliminar($cod);
                }
                
                header('Location: '.URL_BASE.'../index.php?envio=listar');
            }
        }
        
        require_once (RUTA.'/App/Vistas/elimina_envio.php');
    }
    
    /**
     * Función que cambia el estado del envío a "entregado"
     */
    public static function anotarEnvio() {
        $modelo = new ModeloEnvios();
        
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $cod = $_POST['cod'];
            $existe = $modelo->existeEnvio($cod);
            
            if($existe) {
                $modelo->confirmarRecepcion($cod);
            }
            
            header('Location: '.URL_BASE.'../index.php?envio=listar');
        }
        
        require_once (RUTA.'/App/Vistas/anota_recepcion.php');
    }
    
    /**
     * Funcion que permite modificar los apartados en el envío elegido
     */
    public static function modificarEnvio(){
        $modelo = new ModeloEnvios();
        
        $existe = NULL;
        
        if(isset($_POST['buscar'])) {
            $cod = $_POST['codigo'];
            
            $existe = $modelo->existeEnvio($cod);
            
            $datos = array(
                    'destinatario'=>'',
                    'email' =>'',
                    'telefono' =>'',
                    'poblacion' =>'',
                    'provincia' => '',
                    'direccion' =>'',
                    'c_postal' =>'',
                    'observaciones' => ''
                );
            
            $provincias = $modelo->obtenerProvincias();
            
            if(isset($_POST['enviarForm']) && $existe) {
                $envio = $modelo->obtenerDatosModificables($cod);
                
                foreach ($envio as $campo => $post) {
                    if($campo != 'codigo') {
                        if(isset($_POST[$campo])) {
                            $datos[$campo] = $_POST[$campo];
                        }
                        else {
                            $datos[$campo] = $envio[$campo];
                        }
                    }
                }
                
                $error_validacion = FiltroDatos::filtrarEnvio($datos);
                
                if(empty($error_validacion)) {
                    $mensaje = $modelo->editar($envio,$cod);


                    header('Location: '.URL_BASE.'../index.php?envio=listar');
                }
                else {
                    $mensaje = "Los datos introducidos no son válidos.";
                }
            }
            else {
                $error = "El código no es correcto";
            }
        }
        
        require_once (RUTA.'/App/Vistas/modifica_envio.php');
    }
    
    
}
