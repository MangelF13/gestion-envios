<?php

ob_start();

?>



<?php
if(is_array($envios)) {
    
    ?>
<table class="lista">
    <tr>
        <td>Cód. envío</td>
        <td>Destinatario</td>
        <td>Fecha de envío</td>
        <td>Estado del pedido</td>
        <td>Fecha de entrega</td>
        <td style="text-align:center;">Email</td>
        <td>Teléfono</td>
        <td>Población</td>
        <td>Provincia</td>
        <td style="text-align:center;">Dirección</td>
        <td>Código Postal</td>
        <td>Observaciones</td>
    </tr>
    <?php
    foreach ($envios as $envio) {
        $string = "<tr>";
                if(isset($envio['codigo_envio']))
                    $string .= "<td class='even' style=".'"'."text-align:center;".'"'.">".$envio['codigo_envio']."</td>";

                if(isset($envio['destinatario']))
                    $string .= "<td class='odd'>".$envio['destinatario']."</td>";

                if(isset($envio['fechaEnvio']))
                    $string .= "<td class='even' style=".'"'."text-align:center;".'"'.">".$envio['fechaEnvio']."</td>";

                if(isset($envio['estado'])) {
                    if($envio['estado'] == 'E') {
                        $string .= "<td class='odd' style=".'"'."text-align:center;".'"'.">Entregado</td>";
                    }
                    else{
                        if($envio['estado'] == 'P') {
                            $string .= "<td class='odd' style=".'"'."text-align:center;".'"'.">Pedido</td>";
                        }
                        else {
                            $string .= "<td class='odd' style=".'"'."text-align:center;".'"'.">Anulado</td>";
                        }
                    }
                    if($envio['estado'] == 'E' && isset($envio['fechaEntrega'])) {
                        $string .= "<td class='even' style=".'"'."text-align:center;".'"'.">".$envio['fechaEntrega']."</td>";
                    }
                    else {
                        $string .= "<td class='even'></td>";
                    }
                }



                if(isset($envio['email'])) {
                    $string .= "<td class='odd'>".$envio['email']."</td>";
                }

                if(isset($envio['telefono'])) {
                    $string .= "<td class='even'>".$envio['telefono']."</td>";
                }

                if(isset($envio['poblacion'])) {
                    $string .= "<td class='odd'>".$envio['poblacion']."</td>";
                }

                if(isset($envio['provincia'])){
                    $string .= "<td class='even'>".$envio['provincia']."</td>";
                }

                if(isset($envio['direccion'])) {
                    $string .= "<td class='odd'>".  ($envio['direccion'])."</td>";
                }

                if(isset($envio['c_postal'])) {
                    $string .= "<td class='even' style=".'"'."text-align:center;".'"'.">".$envio['c_postal']."</td>";
                }

                if(!empty($envio['observaciones'])) {
                    $string .= "<td class='odd'>".$envio['observaciones']."</td>";
                }

        $string .= "</tr>";

        echo $string;
    }
}
else {
    echo "No hay envíos que mostrar";
}
?>
</table>


<?php
if(isset($mensaje)) {
    echo "<br /><p class='mensaje'>".$mensaje."</p><br />";
}



/*
if ($numeroPaginas > 1) {
    if ($pagina != 1) {
        echo '<a href="'.URL_BASE.'/App/index.php?operacion=listar&pagina=1"><<</a><a href="'.URL_BASE.'/App/index.php?operacion=listar&pagina='.'">>></a>';
    }
    else {
        echo 'Única página';
    }
}
*/



$contenido = ob_get_clean();
$titulo = "Lista de envíos";
$cabecera = "Mostrando lista de envíos";

require_once 'base.php';