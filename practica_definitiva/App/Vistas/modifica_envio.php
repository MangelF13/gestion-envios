<?php

ob_start();

?>

<form method="POST" action="">
    <label>Busqueda del envío a modificar</label>
    <input type="number" name="codigo" value="<?= $cod?>" >
    <br />
    <input type="submit" name="buscar" value="Buscar">
    <br />
    <p>Elige los campos a modificar: </p>
    <input type="checkbox" name="check_list[]" value="destinatario" /> Destinatario <br/>
    <input type="checkbox" name="check_list[]" value="email" /> Email <br/>
    <input type="checkbox" name="check_list[]" value="telefono" /> Teléfono <br/>
    <input type="checkbox" name="check_list[]" value="poblacion" /> Población <br/>
    <input type="checkbox" name="check_list[]" value="provincia" /> Provincia <br/>
    <input type="checkbox" name="check_list[]" value="dirección" /> Dirección <br/>
    <input type="checkbox" name="check_list[]" value="c_postal" /> Código postal <br/>
    <input type="checkbox" name="check_list[]" value="observaciones" /> Observaciones <br/>
    <br />
    <br />
<?php
if($existe && !empty($_POST['codigo'])) {
    if(!empty($_POST['check_list'])) {
        foreach ($_POST['check_list'] as $campo) {
            if($campo == "destinatario") {
?>
    <br />
    <label for="destinatario">Destinatario</label>
    <input <?php echo(isset($error_validacion["destinatario-error"]))? "style='color: #FF0000'" : "";?> type="text" name="destinatario" id="destinatario" value="<?=isset($datos["destinatario"])? $datos["destinatario"] : "" ?>" required/>
<?php
            }
            if($campo == "email") {
?>
    <br />
    <label for="email">Email</label>
    <input <?php echo(isset($error_validacion["email-error"]))? "style='color: #FF0000'" : "";?> type="text" name="email" id="email" value="<?=isset($datos["email"])? $datos["email"] : "" ?>" required/>
<?php
            }
            if($campo == "telefono") {
?>
    <br />
    <label for="telefono">Teléfono</label>
    <input <?php echo(isset($error_validacion["telefono-error"]))? "style='color: #FF0000'" : "";?> type="text" name="telefono" id="telefono" value="<?=isset($datos["telefono"])? $datos["telefono"] : "" ?>" required/>
<?php
            }
            if($campo == "poblacion") {
?>
    <br />
    <label for="poblacion">Población</label>
    <input <?php echo(isset($error_validacion["poblacion-error"]))? "style='color: #FF0000'" : "";?> type="text" name="poblacion" id="poblacion" value="<?=isset($datos["poblacion"])? $datos["poblacion"] : ""?>" required/>
<?php
            }
            if($campo == "provincia") {
?>
    <br />
    <label for="provincia">Provincia: </label>
    <select  id="provincia" name="provincia" required>
        <option <?php echo(isset($error_validacion["provincia-error"]))? "style='color: #FF0000'" : "";?> value="<?=isset($datos["provincia"])? $datos["provincia"] : "" ?>"></option>
        <?php 
        foreach ($provincias as $provincia) {
            if(!isset($provincia['cod']) && !isset($provincia["nombre"])) {
                break;
            }
            echo "<option value='".$provincia['cod']."'>".$provincia["nombre"]."</option>";
        } 
        ?>
    </select>
<?php
            }
            if($campo == "direccion") {
?>
    <br />
    <label for="direccion">Dirección</label>
    <input <?php echo(isset($error_validacion["direccion-error"]))? "style='color: #FF0000'" : "";?> type="text" name="direccion" id="direccion" value="<?=isset($datos["direccion"])? $datos["direccion"] : "" ?>" required/>
<?php
            }
            if($campo == "c_postal") {
?>
    <br />
    <label for="cod_postal">Código postal</label>
    <input <?php echo(isset($error_validacion["c_postal-error"]))? "style='color: #FF0000'" : "";?> type="number" name="c_postal" id="c_postal" value="<?=isset($datos["c_postal"])? $datos["c_postal"] : "" ?>" required/>
<?php
            }
            if($campo == "observaciones") {
?>
    <br />
    <textarea rows="5" cols="50" name="observaciones" placeholder="Escriba aquí cualquier información adicional acerca del envío"><?=isset($datos["observaciones"])? $datos["observaciones"] : "" ?></textarea>

<?php
            }
        }
    }
    ?>
    <br/>
    <input type="hidden" name="enviarForm" value="enviarForm" />
    <input type="submit" name="modificar" value="Modificar" />
    <?php 
    if(isset($mensaje)) {
        echo "<br /><p class='mensaje'>".$mensaje."</p><br />";
        echo "$error";
    }
}
?>
</form>
<?php
$contenido = ob_get_clean();
$titulo = "Modificar envío";
$cabecera = "Elige el envío a modificar:";

require_once 'base.php';