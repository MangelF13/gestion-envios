<?php

ob_start();

?>
<form method="POST" action="">
    <label for="eliminar">Código del envío: </label>
    <input type="number" name="eliminar" id="eliminar" value="<?= $cod?>" required/>
    <input type='submit' name='borrar' value='Borrar'/>
    <br />
    <?php 
    if(isset($error)) {
        echo "El envío seleccionado no existe.";
    }
    if(isset($accion)) {
        if($accion == 'confirmar') {
            echo "<br /><p>¿Desea borrar el envío número ".$cod."?</p>";
            echo "<input type='submit' name='confirmar' value='Sí'/> <input type='submit' name='confirmar' value='No'/>";
            echo "<input type='hidden' name='codigo' value='<?= $cod?>'/>";
        }
    }
    ?>
</form>
<?php

$contenido = ob_get_clean();
$titulo = "Eliminar envío";
$cabecera = "Elija el envío que desea eliminar:";

require_once 'base.php';