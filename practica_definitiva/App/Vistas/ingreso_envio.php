<?php

ob_start();

?>

<form method="POST" action="">
    <label for="destinatario">Destinatario</label>
    <input <?php echo(isset($error_validacion["destinatario-error"]))? "style='color: #FF0000'" : "";?> type="text" name="destinatario" id="destinatario" value="<?=$datos["destinatario"]?>" required/>
    <br />
    <label for="email">Email</label>
    <input <?php echo(isset($error_validacion["email-error"]))? "style='color: #FF0000'" : "";?> type="text" name="email" id="email" value="<?=$datos["email"]?>" required/>
    <br />
    <label for="telefono">Teléfono</label>
    <input <?php echo(isset($error_validacion["telefono-error"]))? "style='color: #FF0000'" : "";?> type="text" name="telefono" id="telefono" value="<?=$datos["telefono"]?>" required/>
    <br />
    <label for="poblacion">Población</label>
    <input <?php echo(isset($error_validacion["poblacion-error"]))? "style='color: #FF0000'" : "";?> type="text" name="poblacion" id="poblacion" value="<?=$datos["poblacion"]?>" required/>
    <br />
    <label for="provincia">Provincia: </label>
    <select  id="provincia" name="provincia" required>
        <option <?php echo(isset($error_validacion["provincia-error"]))? "style='color: #FF0000'" : "";?> value="<?=$datos["provincia"]?>"></option>
        <?php 
        foreach ($provincias as $provincia) {
            if(!isset($provincia['cod']) && !isset($provincia["nombre"])) {
                break;
            }
            echo "<option value='".$provincia['cod']."'>".$provincia["nombre"]."</option>";
        } 
        ?>
    </select>
    <br />
    <label for="direccion">Dirección</label>
    <input <?php echo(isset($error_validacion["direccion-error"]))? "style='color: #FF0000'" : "";?> type="text" name="direccion" id="direccion" value="<?=$datos["direccion"]?>" required/>
    <br />
    <label for="cod_postal">Código postal</label>
    <input <?php echo(isset($error_validacion["c_postal-error"]))? "style='color: #FF0000'" : "";?> type="number" name="c_postal" id="c_postal" value="<?=$datos["c_postal"]?>" required/>
    <br />
    <textarea rows="5" cols="50" name="observaciones" placeholder="Escriba aquí cualquier información adicional acerca del envío"><?=$datos["observaciones"]?></textarea>
    <br/>
    <input type="submit" value="Continuar" />
    <?php 
    if(isset($datos['mensaje'])) {
        echo "<br /><p class='mensaje'>".$datos["mensaje"]."</p><br />";
    }
    ?>
</form>

<?php

$contenido = ob_get_clean();
$titulo = "Nuevo envío";
$cabecera = "Introduciendo un nuevo envío";

require_once 'base.php';