<?php

/*
if(isset($_COOKIE['estilo'])) {
    $estilo = $_COOKIE['estilo'];
}*/

    if(isset($_GET['estilo'])) {
        $estilo = $_GET['estilo'];

        if ($estilo == "alternativo") {
            $estilo = "index";
        }
        else {
            $estilo = "alternativo";
        }
    }
    else {
        $estilo = "index";
    }

/*
if(isset($estilo)) {
    setcookie('estilo', $estilo);
}*/
?>
<!DOCTYPE html>
<html>
<head>
    <meta  http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title><?= isset($titulo)? $titulo : "Inicio" ?></title>
    <link rel='stylesheet' type='text/css' href='../Assets/css/<?php echo $estilo; ?>.css' />
</head>
<body>
    <div class="cabecera">
        <div class="logo">KeNoLlega S.L</div>
        <div class="botones">
            <a href="<?= URL_BASE?>../index.php?estilo=alternativo">Tema básico</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?= URL_BASE?>../index.php?estilo=index">Tema alternativo</a>
        </div>
    </div>
    <div class="contenedor">
        <nav>
            <ul>
                <?php if(!isset($estilo) || $estilo == 'index') {?>
                <li><a href="<?= URL_BASE?>../index.php">Inicio</a></li>
                <li><a href="<?= URL_BASE?>../index.php?envio=ingresar">Crear envío</a></li>
                <li><a href="<?= URL_BASE?>../index.php?envio=listar">Listar envíos</a></li>
                <li><a href="<?= URL_BASE?>../index.php?envio=eliminar">Eliminar envío</a></li>
                <li><a href="<?= URL_BASE?>../index.php?envio=anotar">Anotar recepción</a></li>
                <li><a href="<?= URL_BASE?>../index.php?envio=modificar">Modificar envío</a></li>
                <?php }
                      else {
                ?>
                <li><a href="<?= URL_BASE?>../index.php?estilo=<?= isset($estilo)? 'index' : ""?>">Inicio</a></li>
                <li><a href="<?= URL_BASE?>../index.php?envio=ingresar&estilo=<?= isset($estilo)? 'index' : ""?>">Crear envío</a></li>
                <li><a href="<?= URL_BASE?>../index.php?envio=listar&estilo=<?= isset($estilo)? 'index' : ""?>">Listar envíos</a></li>
                <li><a href="<?= URL_BASE?>../index.php?envio=eliminar&estilo=<?= isset($estilo)? 'index' : ""?>">Eliminar envío</a></li>
                <li><a href="<?= URL_BASE?>../index.php?envio=anotar&estilo=<?= isset($estilo)? 'index' : ""?>">Anotar recepción</a></li>
                <li><a href="<?= URL_BASE?>../index.php?envio=modificar&estilo=<?= isset($estilo)? 'index' : ""?>">Modificar envío</a></li>
                <?php
                      }
                ?>
            </ul>
        </nav>
        <section class="menu">
            <div class="titulo"><?= isset($titulo)? "<p><h2><i>".($cabecera)."</i></h2></p>" : "<p>Práctica de gestíon de envíos</p>" ?></div>
            <div class="contenido"><?= isset($contenido)? ($contenido) : "" ?></div>
        </section>
    </div>
</body>
</html>
<?php

