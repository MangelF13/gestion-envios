<?php


class ModeloEnvios {
    
    public $conexion;
    
    
    public function __construct() {
        $conexion = ConexionBDD::crearConexion();
        
        $this->conexion = $conexion;
    }
    
    /**
     * Inserta un envío
     * 
     * @param type $datos
     * 
     * @return string
     */
    public function insertarEnvio($datos) {
        $campos = [];
        $filas = [];
        foreach($datos as $indice => $valor) {
            $filas[] = $indice;
            if ($indice == 'fecha_envio') {
                $campos['fecha_envio']="CURDATE()";
            }
            else {
                $campos[$indice]="'$valor'";
            }
        }
        
        
        $filas = implode(",", $filas);
        $campos = implode(",", $campos);
        
        $consulta = "Insert into envios ($filas) values ($campos)";
        
        $resultado = $this->conexion->enviarConsulta($consulta);
        
        if($resultado) {
            $mensaje = "Insertado envío nº".$this->conexion->ultimoId();
        }
        else {
            $mensaje = "No se pudo realizar la inserción del envio"."<br />".$this->conexion->obtenerError();
        }
        
        return $mensaje;
    }
    
    
    /**
     * Obtiene el numero de envios existentes (ayudará con las vistas)
     *
     * @param $tabla
     * @param $condiciones
     *
     * @return mixed
     */
    public function obtenerNumeroEnvios($condiciones) {
        $consulta = "Select count(*) as cantidad from envios";
        
        if($condiciones) {
            $consulta .= " where $condiciones";
        }
        
        return $this->conexion->ejecutarUnico($consulta);
    }
    
    
    /**
     * Edita un envío
     *
     * @param $datos
     * @param $codigo
     *
     * @return string
     */
    public function editar($datos, $cod) {
        $campos = [];
        foreach ($datos as $clave => $fila) {

            $fila = "'$fila'";
            $campos[] = "$clave = $fila";
        }
        $campos = implode(",", $campos);

        $consulta = "update envios set $campos where codigo_envio = $cod";

        $consultaRealizada=$this->conexion->enviarConsulta($consulta);
        if($consultaRealizada) {
            $mensaje="Envio modificado correctamente.";
        }
        else {
            $mensaje="No se pudo modificar el envio.";
        }
		
        return $mensaje;
    }
    
    
    /**
     * Obtiene todos los envios que cumplen los datos referentes a la paginacion y las condiciones.
     *
     * @param $inicio
     * @param $tamanoPagina
     * @param $condiciones
     *
     * @return mixed
     */
    public function obtenerEnvios($inicio, $tamanoPagina, $condiciones) {
        $consulta = "select *,DATE_FORMAT(fecha_envio,'%d/%m/%Y')as fechaEnvio, DATE_FORMAT(fecha_entrega,'%d/%m/%Y')as fechaEntrega from envios";
        $sqlordenar = " ORDER BY fechaEnvio,codigo_envio LIMIT ".$inicio.",".$tamanoPagina;
        
        if($condiciones) {
            $consulta .= " where $condiciones";
        }
        
        $consulta .= $sqlordenar;
        
        $envios = $this->conexion->ejecutar($consulta);
        
        return $envios;
    }
    
    
    /**
     * Verifica un envio.
     *
     * @param $codigoEnvio
     *
     * @return mixed
     */
    public function existeEnvio($codigoEnvio) {
        $consulta="select codigo_envio from envios where codigo_envio=$codigoEnvio";
        
        return $this->conexion->ejecutarUnico($consulta);
    }
    
    
    /**
     * Elimina un envio
     *
     * @param $codigo
     *
     * @return string
     */
    public function eliminar($codigo) {
        $consulta = "DELETE FROM envios WHERE codigo_envio=$codigo";
        $consultaRealizada = $this->conexion->enviarConsulta($consulta);
        
        if($consultaRealizada) {
            $mensaje = "Envio $codigo eliminado con exito";
        }
        else {
            $mensaje = "No se pudo eliminar el envio";
        }
        
        return $mensaje;
    }
    
    
    
    /**
     * Obtiene un array con todas las provincias.
     *
     * @return mixed
     */
    public function obtenerProvincias() {
        
        $consulta = "select cod,nombre from provincias";
        $provincias = $this->conexion->ejecutar($consulta);
        
        foreach($provincias as $valor) {
            $provincias['nombre'] = utf8_encode($valor['nombre']);
        }
        
        return $provincias;
    }
    
    
    
    /**
     * Obtiene el nombre de una provincia dado su codigo.
     *
     * @param $codProvincia
     *
     * @return string
     */
    public function obtenerNomProvincia($codProvincia) {
        $consulta = "select nombre from provincias where cod=$codProvincia";
        $resultado = $this->conexion->ejecutarUnico($consulta);
        
        return utf8_encode($resultado['nombre']);
    }
    
    
    /**
     * Funcion que devuelve los parametros de busqueda
     *
     * @param null $criterios
     *
     * @return array|string
     */
    public function obtenerCond($condiciones = NULL) {
        $criterios = [];
        
        if($condiciones != NULL) {
            foreach ($condiciones as $condicion) {
                if($condicion['conector']=='like') {
                    $criterios[] = $condicion['campo'].' '.$condicion['conector']."'%".$condicion['valor']."%'";
                }
                else {
                    $criterios[] = $condicion['campo'].' '.$condicion['conector']."'".$condicion['valor']."'";
                }
            }
        }
        
        $criterios = implode(" and ", $criterios);
        
        return $criterios;
    }
    
    
    /**
     * Confirma la llegada de un envio dado su codigo
     *
     * @param $cod
     *
     * @return string
     */
    public function confirmarRecepcion($cod) {
        $consulta = "update `envios` set `estado` = 'E', `fecha_entrega` = CURDATE() where `codigo_envio` =".$cod.";";
        $consultaRealizada = $this->conexion->enviarConsulta($consulta);
        
        if($consultaRealizada) {
            $mensaje = "Envío ".$cod.' anotado';
        }
        else {
            $mensaje = "No se pudo anotar el envío";
        }
        
        return $mensaje;
    }
    
    /**
     * Obtiene los datos modificables de un envio
     *
     * @param $cod
     *
     * @return mixed
     */
    public function obtenerDatosModificables($cod) {
        $consulta="select destinatario,telefono,direccion,cp,poblacion,provincia,email,observaciones from envios where codigo_envio=$cod";
        
        return $this->conexion->ejecutarUnico($consulta);
    }
}